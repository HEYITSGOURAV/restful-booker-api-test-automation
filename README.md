# Restful-Broker-API-Test Automation

## **Overview:**
This API framework is developed using REST Assured and Cucumber.  REST Assured is a Java library that provides a domain-specific language (DSL) for writing powerful, maintainable tests for RESTful APIs. Cucumber is an open source library, which supports behavior driven development. To be more precise, Cucumber can be defined as a testing framework, driven by plain English text. It serves as documentation, automated tests, and a development aid – all in one.

All the test cases are done based on [Restful-booker](https://restful-booker.herokuapp.com/apidoc/index.html).
Please note only positive scenario are covered as per requirement document

### **Some of the key features of this framework:**

1. It generates Cucumber Extent report with all the step details.
2. Generates execution logs, with detailed request and response details.
3. This also has an example to validate response body using json schema and java pojo classes.
5. Test execution can be triggered form command line. 
6. Easy integration to CI/CD pipeline.

## **Required Setup :**

- Java should be installed and configured.
- Maven should be installed and configured.

## **Running Test:**

Open the command prompt and navigate to the folder in which pom.xml file is present.
Run the below Maven command.

    mvn clean install


Once the execution completes report & log will be generated in below folder.

**Report:** 		*target/cucumber-report*<br>
**Log:** 		*target/logs*